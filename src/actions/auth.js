import {
    ACTION_CHANGE_FIRST_NAME,
    ACTION_CHANGE_LAST_NAME,
    ACTION_CHANGE_PASSWORD,
    CLEAR_AUTH_FIELDS,
} from '../constants';

export function changeFirstName(newFirstName) {
    return {
        type: ACTION_CHANGE_FIRST_NAME,
        payload: newFirstName,
    };
}

export function changeLastName(newLastName) {
    return {
        type: ACTION_CHANGE_LAST_NAME,
        payload: newLastName,
    };
}

export function changePassword(newPassword) {
    return {
        type: ACTION_CHANGE_PASSWORD,
        payload: newPassword,
    };
}

export function clearFields() {
    return {
        type: CLEAR_AUTH_FIELDS,
    };
}
