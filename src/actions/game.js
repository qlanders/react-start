import {
    TOGGLE_BOX,
} from '../constants';

export function toggleBox() {
    return {
        type: TOGGLE_BOX,
    };
}
