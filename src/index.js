/* eslint-disable react/jsx-filename-extension */

import React from 'react';
import { render } from 'react-dom';

import Root from './root';
import './components/common/styles/main.scss';

const mounthNode = document.getElementById('root');
render(<Root />, mounthNode);

if (module.hot) {
    module.hot.accept();
}
