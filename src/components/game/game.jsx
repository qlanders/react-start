import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CSSTransition from 'react-addons-css-transition-group';
import './game.styl';

import * as gameActions from '../../actions/game';


class Game extends Component {
    createEl = () => {
        const { showBox } = this.props;

        if (showBox) {
            return (
                <span className="game__box">
                    {Math.random()}
                </span>
            );
        }

        return null;
    };

    render() {
        const { toggleBox } = this.props;

        return (
            <>
                <CSSTransition
                    transitionName="game__box"
                    transitionEnterTimeout={3000}
                    transitionLeaveTimeout={3000}
                >
                    {this.createEl()}
                </CSSTransition>
                <button
                    type="button"
                    className="btn btn-success"
                    onClick={toggleBox}
                >
                Toggle Box
                </button>
            </>
        );
    }
}

Game.propTypes = {
    showBox: PropTypes.bool.isRequired,
    toggleBox: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    showBox: state.game.showBox,
});

const mapDispatchToProps = { ...gameActions };

export default connect(mapStateToProps, mapDispatchToProps)(Game);
