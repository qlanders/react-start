import React, { Component } from 'react';
import pause from './images/pause.svg';
import play from './images/play.svg';
import Svg from '../../hoc/svg/svg';

class SvgTest extends Component {
    render() {
        return (
            <div>
                <Svg
                    symbol={pause}
                    params={{ height: '15', width: '15' }}
                />
                <Svg
                    symbol={play}
                    params={{ height: '20', width: '15' }}
                />
            </div>
        );
    }
}

export default SvgTest;
