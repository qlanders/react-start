import React, { Component } from 'react';
import { Link, Route, Switch } from 'react-router-dom';

import Home from '../home/home';
import Auth from '../auth/auth';
import Game from '../game/game';
import News from '../news/news';
import Test from '../test/test';
import SvgTest from '../svg-test/svg-test';
import Conditions from '../conditions/conditions';


class App extends Component {
    componentWillMount() {
        const [{ id }] = this.getListItems().filter(page => page.link === window.location.pathname);

        this.setState({ activeId: id });
    }

    getListItems = () => ([
        {
            name: 'Home',
            link: '/',
            id: 0,
        },
        {
            name: 'Auth',
            link: '/auth',
            id: 1,
        },
        {
            name: 'Conditions',
            link: '/conditions',
            id: 2,
        },
        {
            name: 'Game',
            link: '/game',
            id: 3,
        },
        {
            name: 'News',
            link: '/news',
            id: 4,
        },
        {
            name: 'Test',
            link: '/test',
            id: 5,
        },
        {
            name: 'svg',
            link: '/svg',
            id: 6,
        },
    ]);

    handleActiveLink = (id) => {
        this.setState({ activeId: id });
    };

    renderList = () => (
        this.getListItems().map((item) => {
            const { name, link, id } = item;
            let activeClass = '';
            const { activeId } = this.state;
            if (id === activeId) activeClass = ' active';

            return (
                <li key={id} className="nav-item">
                    <Link
                        to={link}
                        className={`nav-link${activeClass}`}
                        onClick={() => this.handleActiveLink(id)}
                    >
                        {name}
                    </Link>
                </li>
            );
        })
    );

    render() {
        return (
            <>
                <nav className="container">
                    <ul className="nav nav-tabs">
                        {this.renderList()}
                    </ul>
                </nav>

                <main className="container">
                    <div className="row">
                        <div className="col-12 pt-4 pb-4">
                            <Switch>
                                <Route path="/auth" component={Auth} />
                                <Route path="/conditions" component={Conditions} />
                                <Route path="/game" component={Game} />
                                <Route path="/news" component={News} />
                                <Route path="/test" component={Test} />
                                <Route path="/svg" component={SvgTest} />
                                <Route exact path="/" component={Home} />
                            </Switch>
                        </div>
                    </div>
                </main>
            </>
        );
    }
}

export default App;
