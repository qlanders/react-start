import React, { Component } from 'react';
import PropTypes from 'prop-types';


class ParentClass extends Component {
    logColor = (title, color) => {
        console.log('---', title, color);
    };

    render() {
        return (
            <AddColorForm onNewColor={this.logColor} />
        );
    }
}

class AddColorForm extends Component {
    constructor(props) {
        super(props);
        this.title = React.createRef();
        this.color = React.createRef();
    }

    submit = (event) => {
        event.preventDefault();
        const { onNewColor } = this.props;

        onNewColor(this.title.current.value, this.color.current.value);

        this.title.current.value = '';
        this.color.current.value = '#000';
        this.title.current.focus();
    };

    render() {
        return (
            <form onSubmit={this.submit} className="form-group">
                <input
                    className="form-control"
                    type="text"
                    ref={this.title}
                    placeholder="color title"
                    required
                />
                <input
                    className="form-control"
                    type="text"
                    ref={this.color}
                    placeholder="color"
                    required
                />
                <button
                    type="submit"
                    className="btn btn-success"
                >
                    Add
                </button>
            </form>
        );
    }
}

AddColorForm.propTypes = {
    onNewColor: PropTypes.func,
};

AddColorForm.defaultProps = {
    onNewColor: f => f,
};

export default ParentClass;
