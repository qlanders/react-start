import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import './auth.styl';

import * as authActions from '../../actions/auth';

class Auth extends Component {
    changeFirstNameValue = (event) => {
        const { changeFirstName } = this.props;
        changeFirstName(event.target.value);
    };

    changeLastNameValue = (event) => {
        const { changeLastName } = this.props;
        changeLastName(event.target.value);
    };

    changePasswordValue = (event) => {
        const { changePassword } = this.props;
        changePassword(event.target.value);
    };

    clearAllFields = () => {
        const { clearFields } = this.props;
        clearFields();
    };

    onSubmit = (event) => {
        const { firstName, lastName, password } = this.props;
        event.preventDefault();

        console.log('---', firstName, lastName, password);
    };

    render() {
        const { firstName, lastName, password } = this.props;

        return (
            <form>
                <div className="auth__item form-group">
                    <input
                        type="text"
                        value={firstName}
                        className="auth__input form-control"
                        placeholder="First Name"
                        onChange={this.changeFirstNameValue}
                    />
                </div>
                <div className="auth__item form-group">
                    <input
                        type="text"
                        value={lastName}
                        className="auth__input form-control"
                        placeholder="Last Name"
                        onChange={this.changeLastNameValue}
                    />
                </div>
                <div className="auth__item form-group">
                    <input
                        type="password"
                        value={password}
                        className="auth__input form-control"
                        placeholder="Password"
                        onChange={this.changePasswordValue}
                    />
                </div>
                <div className="form-group">
                    <span>{`${firstName} ${lastName}`}</span>
                </div>
                <button
                    type="button"
                    className="btn btn-warning"
                    onClick={this.clearAllFields}
                >
                    Clear
                </button>
                <button
                    type="button"
                    className="btn btn-primary"
                    onClick={this.onSubmit}
                >
                    Submit!
                </button>
            </form>
        );
    }
}

Auth.propTypes = {
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    changeFirstName: PropTypes.func.isRequired,
    changeLastName: PropTypes.func.isRequired,
    changePassword: PropTypes.func.isRequired,
    clearFields: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    firstName: state.auth.firstName,
    lastName: state.auth.lastName,
    password: state.auth.password,
});

const mapDispatchToProps = { ...authActions };

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
