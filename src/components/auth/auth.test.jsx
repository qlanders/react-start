import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import Auth from './auth';
import store from '../../store';

describe('Auth component', () => {
    it('test', (done) => {
        const wrapper = mount(
            <Provider store={store}>
                <Auth />
            </Provider>,
        );

        const el = wrapper.find('.auth__input').at(0);
        el.simulate('change', { target: { value: 'foo' } });

        expect(el.value).toEqual('foo').done();
    });
});
