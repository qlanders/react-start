import React from 'react';

const Svg = (props) => {
    const { symbol, params } = props;
    const { id, viewBox } = symbol;

    return (
        <svg viewBox={viewBox} {...params}>
            <use xlinkHref={`#${id}`} />
        </svg>
    );
};

export default Svg;
