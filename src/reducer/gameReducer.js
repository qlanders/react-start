import { TOGGLE_BOX } from '../constants';

const initialState = {
    showBox: false,
};

export default (state = initialState, action) => {

    switch (action.type) {
    case TOGGLE_BOX:
        return { showBox: !state.showBox };

    default:
        return state;
    }
};
