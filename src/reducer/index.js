import { combineReducers } from 'redux';
import authReducer from './authReducer';
import gameReducer from './gameReducer';

const reducer = combineReducers({
    auth: authReducer,
    game: gameReducer,
});

export default reducer;
