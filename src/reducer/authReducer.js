import {
    ACTION_CHANGE_FIRST_NAME,
    ACTION_CHANGE_LAST_NAME,
    ACTION_CHANGE_PASSWORD,
    CLEAR_AUTH_FIELDS,
    // TOGGLE_BOX,
} from '../constants';

const initialState = {
    firstName: '',
    lastName: '',
    password: '',
    error: '',
    success: false,
};

export default (state = initialState, action) => {
    const { payload, type } = action;

    switch (type) {
    case ACTION_CHANGE_FIRST_NAME:
        return { ...state, firstName: payload };

    case ACTION_CHANGE_LAST_NAME:
        return { ...state, lastName: payload };

    case ACTION_CHANGE_PASSWORD:
        return { ...state, password: payload };

    case CLEAR_AUTH_FIELDS:
        return { ...initialState };

    default:
        return state;
    }
};
