module.exports = (api) => {
    api.cache(true);

    const presets = [
        ['@babel/preset-env',
            {
                targets: {
                    browsers: ['> 1%', 'last 5 versions', 'ie > 9'],
                },
            }],
        '@babel/preset-react',
    ];

    const plugins = [
        '@babel/plugin-proposal-class-properties',
    ];

    return {
        presets,
        plugins,
    };
};
